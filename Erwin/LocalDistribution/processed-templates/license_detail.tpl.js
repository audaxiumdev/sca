define('license_detail.tpl', ['Handlebars','Handlebars.CompilerNameLookup'], function (Handlebars, compilerNameLookup){ var template = Handlebars.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, helper, helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression, functionType="function", lambda=this.lambda;
  return " <section class=\"case-detail\"><header class=\"case-detail-title\"><a href=\"/cases\" class=\"case-detail-back-btn\">"
    + escapeExpression(((compilerNameLookup(helpers,"translate") || (depth0 && compilerNameLookup(depth0,"translate")) || helperMissing).call(depth0, "&lt; Back to Cases", {"name":"translate","hash":{},"data":data})))
    + "</a><h2 class=\"case-detail-header-title\"><span class=\"case-detail-field-number\">"
    + escapeExpression(((helper = (helper = compilerNameLookup(helpers,"pageHeader") || (depth0 != null ? compilerNameLookup(depth0,"pageHeader") : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"pageHeader","hash":{},"data":data}) : helper)))
    + "</span><span class=\"case-detail-field-subject\"> "
    + escapeExpression(lambda(((stack1 = (depth0 != null ? compilerNameLookup(depth0,"model") : depth0)) != null ? compilerNameLookup(stack1,"title") : stack1), depth0))
    + "</span></h2></header><div data-confirm-message class=\"case-detail-confirm-message\"></div><div data-type=\"alert-placeholder\"></div> "
    + escapeExpression(((compilerNameLookup(helpers,"debug") || (depth0 && compilerNameLookup(depth0,"debug")) || helperMissing).call(depth0, (depth0 != null ? compilerNameLookup(depth0,"model") : depth0), {"name":"debug","hash":{},"data":data})))
    + " <div class=\"case-detail-header-information\"><div class=\"case-detail-header-row\"><div class=\"case-detail-header-col-left\"></div></div><div class=\"\"><ul class=\"list-group\"><li class=\"list-group-item\"><b>Original Sales Order #</b>: "
    + escapeExpression(lambda(((stack1 = (depth0 != null ? compilerNameLookup(depth0,"model") : depth0)) != null ? compilerNameLookup(stack1,"salesorder") : stack1), depth0))
    + "</li><li class=\"list-group-item\"><b>Distributor PO#</b>: "
    + escapeExpression(lambda(((stack1 = (depth0 != null ? compilerNameLookup(depth0,"model") : depth0)) != null ? compilerNameLookup(stack1,"distributorpo") : stack1), depth0))
    + "</li><<<<<<< HEAD\n======= <li class=\"list-group-item\"><b>Item Group</b>: "
    + escapeExpression(lambda(((stack1 = (depth0 != null ? compilerNameLookup(depth0,"model") : depth0)) != null ? compilerNameLookup(stack1,"itemgroup") : stack1), depth0))
    + "</li> >>>>>>> 21f06d8662e52cb2048cbc89b119468abc6e9795 <li class=\"list-group-item\"><b>Start date</b>: "
    + escapeExpression(lambda(((stack1 = (depth0 != null ? compilerNameLookup(depth0,"model") : depth0)) != null ? compilerNameLookup(stack1,"startdate") : stack1), depth0))
    + "</li><li class=\"list-group-item\"><b>End Date</b>: "
    + escapeExpression(lambda(((stack1 = (depth0 != null ? compilerNameLookup(depth0,"model") : depth0)) != null ? compilerNameLookup(stack1,"enddate") : stack1), depth0))
    + "</li><li class=\"list-group-item\"><b>End User</b>: "
    + escapeExpression(lambda(((stack1 = (depth0 != null ? compilerNameLookup(depth0,"model") : depth0)) != null ? compilerNameLookup(stack1,"enduser") : stack1), depth0))
    + "</li><li class=\"list-group-item\"><b>term</b>: "
    + escapeExpression(lambda(((stack1 = (depth0 != null ? compilerNameLookup(depth0,"model") : depth0)) != null ? compilerNameLookup(stack1,"term") : stack1), depth0))
    + "</li><li class=\"list-group-item\"><b>End User Contact</b>: "
    + escapeExpression(lambda(((stack1 = (depth0 != null ? compilerNameLookup(depth0,"model") : depth0)) != null ? compilerNameLookup(stack1,"endusercontact") : stack1), depth0))
    + "</li><<<<<<< HEAD <li class=\"list-group-item\"><b>Status</b>: "
    + escapeExpression(lambda(((stack1 = (depth0 != null ? compilerNameLookup(depth0,"model") : depth0)) != null ? compilerNameLookup(stack1,"status") : stack1), depth0))
    + "</li><li class=\"list-group-item\"><b>Company</b>: "
    + escapeExpression(lambda(((stack1 = (depth0 != null ? compilerNameLookup(depth0,"model") : depth0)) != null ? compilerNameLookup(stack1,"company") : stack1), depth0))
    + "</li><li class=\"list-group-item\"><b>License Code</b>: "
    + escapeExpression(lambda(((stack1 = (depth0 != null ? compilerNameLookup(depth0,"model") : depth0)) != null ? compilerNameLookup(stack1,"licensecode") : stack1), depth0))
    + "</li><li class=\"list-group-item\"><b>Product</b>: "
    + escapeExpression(lambda(((stack1 = (depth0 != null ? compilerNameLookup(depth0,"model") : depth0)) != null ? compilerNameLookup(stack1,"product") : stack1), depth0))
    + "</li><li class=\"list-group-item\"><b>Max Number Active Clients Allowed</b>: "
    + escapeExpression(lambda(((stack1 = (depth0 != null ? compilerNameLookup(depth0,"model") : depth0)) != null ? compilerNameLookup(stack1,"maxactiveclients") : stack1), depth0))
    + "</li><li class=\"list-group-item\"><b>Current Number of Active Clients</b>: "
    + escapeExpression(lambda(((stack1 = (depth0 != null ? compilerNameLookup(depth0,"model") : depth0)) != null ? compilerNameLookup(stack1,"currentactivations") : stack1), depth0))
    + "</li><li class=\"list-group-item\"><b>Maintenance End Date</b>: "
    + escapeExpression(lambda(((stack1 = (depth0 != null ? compilerNameLookup(depth0,"model") : depth0)) != null ? compilerNameLookup(stack1,"maintenanceenddate") : stack1), depth0))
    + "</li> ======= <li class=\"list-group-item\"><b>Reseller</b>: "
    + escapeExpression(lambda(((stack1 = (depth0 != null ? compilerNameLookup(depth0,"model") : depth0)) != null ? compilerNameLookup(stack1,"reseller") : stack1), depth0))
    + "</li><li class=\"list-group-item\"><b>Distributor</b>: "
    + escapeExpression(lambda(((stack1 = (depth0 != null ? compilerNameLookup(depth0,"model") : depth0)) != null ? compilerNameLookup(stack1,"distributor") : stack1), depth0))
    + "</li><li class=\"list-group-item\"><b>Bill To Customer</b>: "
    + escapeExpression(lambda(((stack1 = (depth0 != null ? compilerNameLookup(depth0,"model") : depth0)) != null ? compilerNameLookup(stack1,"billtocustomer") : stack1), depth0))
    + "</li><li class=\"list-group-item\"><b>Status</b>: "
    + escapeExpression(lambda(((stack1 = (depth0 != null ? compilerNameLookup(depth0,"model") : depth0)) != null ? compilerNameLookup(stack1,"status") : stack1), depth0))
    + "</li><li class=\"list-group-item\"><b>Company</b>: "
    + escapeExpression(lambda(((stack1 = (depth0 != null ? compilerNameLookup(depth0,"model") : depth0)) != null ? compilerNameLookup(stack1,"company") : stack1), depth0))
    + "</li><li class=\"list-group-item\"><b>License Code</b>: "
    + escapeExpression(lambda(((stack1 = (depth0 != null ? compilerNameLookup(depth0,"model") : depth0)) != null ? compilerNameLookup(stack1,"licensecode") : stack1), depth0))
    + "</li><li class=\"list-group-item\"><b>Product</b>: "
    + escapeExpression(lambda(((stack1 = (depth0 != null ? compilerNameLookup(depth0,"model") : depth0)) != null ? compilerNameLookup(stack1,"product") : stack1), depth0))
    + "</li><li class=\"list-group-item\"><b>Profile</b>: "
    + escapeExpression(lambda(((stack1 = (depth0 != null ? compilerNameLookup(depth0,"model") : depth0)) != null ? compilerNameLookup(stack1,"profile") : stack1), depth0))
    + "</li><li class=\"list-group-item\"><b>Max Number Active Clients Allowed</b>: "
    + escapeExpression(lambda(((stack1 = (depth0 != null ? compilerNameLookup(depth0,"model") : depth0)) != null ? compilerNameLookup(stack1,"maxactiveclients") : stack1), depth0))
    + "</li><li class=\"list-group-item\"><b>Current Number of Active Clients</b>: "
    + escapeExpression(lambda(((stack1 = (depth0 != null ? compilerNameLookup(depth0,"model") : depth0)) != null ? compilerNameLookup(stack1,"currentactivations") : stack1), depth0))
    + "</li><li class=\"list-group-item\"><b>License Type</b>: "
    + escapeExpression(lambda(((stack1 = (depth0 != null ? compilerNameLookup(depth0,"model") : depth0)) != null ? compilerNameLookup(stack1,"licensetype") : stack1), depth0))
    + "</li><li class=\"list-group-item\"><b>Concurrency</b>: "
    + escapeExpression(lambda(((stack1 = (depth0 != null ? compilerNameLookup(depth0,"model") : depth0)) != null ? compilerNameLookup(stack1,"concurrency") : stack1), depth0))
    + "</li><li class=\"list-group-item\"><b>Maintenance End Date</b>: "
    + escapeExpression(lambda(((stack1 = (depth0 != null ? compilerNameLookup(depth0,"model") : depth0)) != null ? compilerNameLookup(stack1,"maintenanceenddate") : stack1), depth0))
    + "</li><li class=\"list-group-item\"><b>Send to End User Contact</b>: "
    + escapeExpression(lambda(((stack1 = (depth0 != null ? compilerNameLookup(depth0,"model") : depth0)) != null ? compilerNameLookup(stack1,"sendtoendusercontact") : stack1), depth0))
    + "</li><li class=\"list-group-item\"><b>Send to Reseller Contact</b>: "
    + escapeExpression(lambda(((stack1 = (depth0 != null ? compilerNameLookup(depth0,"model") : depth0)) != null ? compilerNameLookup(stack1,"sendtoresellercontact") : stack1), depth0))
    + "</li><li class=\"list-group-item\"><b> Send to Distributor</b>: "
    + escapeExpression(lambda(((stack1 = (depth0 != null ? compilerNameLookup(depth0,"model") : depth0)) != null ? compilerNameLookup(stack1,"sendtodistributor") : stack1), depth0))
    + "</li><li class=\"list-group-item\"><b>Send to Distributor Fulfillment Email Address (Disti Only)</b>: "
    + escapeExpression(lambda(((stack1 = (depth0 != null ? compilerNameLookup(depth0,"model") : depth0)) != null ? compilerNameLookup(stack1,"sendtodistfulfillemail") : stack1), depth0))
    + "</li> >>>>>>> 21f06d8662e52cb2048cbc89b119468abc6e9795 </ul></div></section>  ";
},"useData":true}); template.Name = 'license_detail'; return template;});