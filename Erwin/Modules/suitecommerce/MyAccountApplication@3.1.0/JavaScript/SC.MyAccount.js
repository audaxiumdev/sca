/*
	© 2017 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
*/

/*!
* Description: SuiteCommerce Reference My Account
*
* @copyright (c) 2000-2013, NetSuite Inc.
* @version 1.0
*/

// Application.js
// --------------
// Extends the application with My Account specific core methods
define(
	'SC.MyAccount'
,	[
		'SC.MyAccount.Configuration'
	,	'SC.MyAccount.Layout'

	,	'Application'

	,	'Backbone'
	,	'jQuery'
	,	'underscore'
	,	'Backbone.View'
	,	'MenuTree.View'
	,	'Profile.Model'

	,	'Backbone.Model'
	,	'Backbone.Sync'
	,	'Backbone.Validation.callbacks'
	,	'Backbone.Validation.patterns'
	,	'Backbone.View.render'
	,	'Backbone.View.saveForm'
	,	'Backbone.View.toggleReset'
	,	'Bootstrap.Slider'
	,	'jQuery.ajaxSetup'
	,	'jQuery.serializeObject'

	,	'String.format'
	]
,	function (
		Configuration
	,	MyAccountLayout

	,	Application

	,	Backbone
	,	jQuery
	,	_
	,	BackboneView
	,	MenuTreeView
	,	ProfileModel
	)
{
	'use strict';

	var MyAccount = Application('MyAccount');
	
	var profile = ProfileModel.getInstance();
	// Applies Configuration
	MyAccount.Configuration = _.extend(MyAccount.Configuration, Configuration);

	MyAccount.Layout = MyAccountLayout;

	// Once Modules are loaded this checks if they are exporting a menu entry and if so
	// this adds it to the array of menu entries
	MyAccount.on('afterModulesLoaded', function ()
	{
		var menu_tree = MenuTreeView.getInstance();
		menu_tree.application = MyAccount;

		_.each(MyAccount.modules, function (module)
		{
			if (module)
			{
				if (module.MenuItems)
				{
					menu_tree.addMenuItem(module.MenuItems);
				}
			}
		});
	

	var licenceMenu={
			parent: null
		,	id: 'links'
		,	name: _('Links').translate()
		,	url: 'showLicenseList'
		,	index: 6
		,	children: [
		{
				id: 'product'
			,	parent:'links'
			,	name: "Product Downloads"
			,	url: ''
			,	index: 8
		},
		// {
		// 		id: 'feedback'
		// 	,	parent:'links'
		// 	,	name: "Feedback"
		// 	,	url: 'https://erwin.co1.qualtrics.com/jfe/form/SV_agDiU3JLEpVsr7T'
		// 	,	index: 9
		// },
		// {
		// 		id: 'ActiveSupport'
		// 	,	parent:'links'
		// 	,	name: "Active Support Contracts"
		// 	,	url: 'showCenterContract'
		// 	,	index: 10
		// },
		// {
		// 		id: 'ResellerRegistration'
		// 	,	parent:'links'
		// 	,	name: "Reseller Registration"
		// 	,	url: ''
		// 	,	index: 12
		// }
		]
		}

		

		// var jsonData="";
  //   	var profileModel = profile||null;
    	
		// if(profileModel && profileModel.get('jsonData')){
		// 	jsonData = JSON.parse(profileModel.get('jsonData'));
			
		// 	var counter=1;
		// 	_.each(jsonData.array, function (lic)
		// 	{
				
		// 		console.log(lic)


		// 		licenceMenu.children.push(childLic);
		// 		counter++;
		// 	})
		// }
	    
    	

	 menu_tree.addMenuItem(licenceMenu);
	 });
	setTimeout(function(){
	jQuery('a[data-id="product"]').attr("href","https://erwin.com/order-fulfillment/");
	jQuery('a[data-id="feedback"]').attr("href","https://erwin.co1.qualtrics.com/jfe/form/SV_agDiU3JLEpVsr7T");

	jQuery('a[data-id="product"]').attr("target","_blank");
	jQuery('a[data-id="feedback"]').attr("target","_blank");
	},1000)


	
	// Setup global cache for this application
	jQuery.ajaxSetup({cache:false});

	return MyAccount;
});
