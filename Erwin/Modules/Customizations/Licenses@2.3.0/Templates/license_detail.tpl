{{!
	© 2017 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
}}

<section class="case-detail">
	<header class="case-detail-title">
		<a href="/cases" class="case-detail-back-btn">{{translate '&lt; Back to Cases'}}</a>
		<h2 class="case-detail-header-title">
			<span class="case-detail-field-number">{{pageHeader}}</span>
			<span class="case-detail-field-subject"> {{model.title}}</span>
		</h2>
	</header>

	<div data-confirm-message class="case-detail-confirm-message"></div>

	<div data-type="alert-placeholder"></div>
	{{debug model}}
	<div class="case-detail-header-information">
		<div class="case-detail-header-row">
			<div class="case-detail-header-col-left">

			</div>
	</div>

	<div class="">
		<ul class="list-group">
		  <li class="list-group-item"><b>Original Sales Order #</b>: {{model.salesorder}}</li>
		  <li class="list-group-item"><b>Distributor PO#</b>: {{model.distributorpo}}</li>
<<<<<<< HEAD
=======
		  <li class="list-group-item"><b>Item Group</b>: {{model.itemgroup}}</li>
>>>>>>> 21f06d8662e52cb2048cbc89b119468abc6e9795
		  <li class="list-group-item"><b>Start date</b>: {{model.startdate}}</li>
		  <li class="list-group-item"><b>End Date</b>: {{model.enddate}}</li>
		  <li class="list-group-item"><b>End User</b>: {{model.enduser}}</li>
		  <li class="list-group-item"><b>term</b>: {{model.term}}</li>
		  <li class="list-group-item"><b>End User Contact</b>: {{model.endusercontact}}</li>
<<<<<<< HEAD
			<li class="list-group-item"><b>Status</b>: {{model.status}}</li>
			<li class="list-group-item"><b>Company</b>: {{model.company}}</li>
			<li class="list-group-item"><b>License Code</b>: {{model.licensecode}}</li>
			<li class="list-group-item"><b>Product</b>: {{model.product}}</li>
			<li class="list-group-item"><b>Max Number Active Clients Allowed</b>: {{model.maxactiveclients}}</li>
			<li class="list-group-item"><b>Current Number of Active Clients</b>: {{model.currentactivations}}</li>
			<li class="list-group-item"><b>Maintenance End Date</b>: {{model.maintenanceenddate}}</li>

=======
		  <li class="list-group-item"><b>Reseller</b>: {{model.reseller}}</li>
		  <li class="list-group-item"><b>Distributor</b>: {{model.distributor}}</li>
          <li class="list-group-item"><b>Bill To Customer</b>: {{model.billtocustomer}}</li>
          <li class="list-group-item"><b>Status</b>: {{model.status}}</li>
          <li class="list-group-item"><b>Company</b>: {{model.company}}</li>
          <li class="list-group-item"><b>License Code</b>: {{model.licensecode}}</li>
          <li class="list-group-item"><b>Product</b>: {{model.product}}</li>
          <li class="list-group-item"><b>Profile</b>: {{model.profile}}</li>
          <li class="list-group-item"><b>Max Number Active Clients Allowed</b>: {{model.maxactiveclients}}</li>
          <li class="list-group-item"><b>Current Number of Active Clients</b>: {{model.currentactivations}}</li>
          <li class="list-group-item"><b>License Type</b>: {{model.licensetype}}</li>
          <li class="list-group-item"><b>Concurrency</b>: {{model.concurrency}}</li>
          <li class="list-group-item"><b>Maintenance End Date</b>: {{model.maintenanceenddate}}</li>
          <li class="list-group-item"><b>Send to End User Contact</b>: {{model.sendtoendusercontact}}</li>
          <li class="list-group-item"><b>Send to Reseller Contact</b>: {{model.sendtoresellercontact}}</li>
          <li class="list-group-item"><b>	Send to Distributor</b>: {{model.sendtodistributor}}</li>
          <li class="list-group-item"><b>Send to Distributor Fulfillment Email Address (Disti Only)</b>: {{model.sendtodistfulfillemail}}</li>

		 
>>>>>>> 21f06d8662e52cb2048cbc89b119468abc6e9795
		</ul>
		<!-- <table class="table">
		  <thead>
		    <tr>
		      <th>End User</th>
		      <th>Reseller</th>
		      <th>Order Reference</th>
		      <th>Serial Number</th>
		      <th>Issue Date</th>
		    </tr>
		  </thead>
		  <tbody>
		    <tr>
		      <td>{{model.endUser}}</th>
		      <td>{{model.licensereseller}}</td>
		      <td>{{model.ordref}}</td>	
		      <td>{{model.serialNumber}}</td>  
		      <td>{{model.issueDate}}</td>      
		    </tr>
		  </tbody>
		</table> -->
</div>
<!-- 	<div class="">
		<table class="table">
		  <thead>
		    <tr>	  
		      <th>License End Date</th>
		      <th>Entitlement Date</th>		      
		      <th>Qunatity</th>
		      <th>Product</th>
		      <th>Sales Order Contract</th>
		    </tr>
		  </thead>
		  <tbody>
		    <tr>
		      <td>{{model.lice_end_date}}</td>
		      <td>{{model.version_entitle_date}}</td>
		      <td>{{model.lic_quantity}}</td>		      
		      <td>{{model.cc_product}}</td>
		      <td>{{model.cc_originalcontract}}</td>	      
		      
		    </tr>
		  </tbody>
		</table>
</div> -->


</section>



{{!----
Use the following context variables when customizing this template: 
	
	model (Object)
	model.internalid (String)
	model.caseNumber (String)
	model.title (String)
	model.grouped_messages (Array)
	model.grouped_messages.0 (Object)
	model.grouped_messages.0.date (String)
	model.grouped_messages.0.messages (Array)
	model.grouped_messages.0.messages.0 (Object)
	model.grouped_messages.0.messages.0.author (String)
	model.grouped_messages.0.messages.0.text (String)
	model.grouped_messages.0.messages.0.messageDate (String)
	model.grouped_messages.0.messages.0.internalid (String)
	model.grouped_messages.0.messages.0.initialMessage (Boolean)
	model.status (Object)
	model.status.id (String)
	model.status.name (String)
	model.origin (Object)
	model.origin.id (String)
	model.origin.name (String)
	model.category (Object)
	model.category.id (String)
	model.category.name (String)
	model.company (Object)
	model.company.id (String)
	model.company.name (String)
	model.priority (Object)
	model.priority.id (String)
	model.priority.name (String)
	model.createdDate (String)
	model.lastMessageDate (String)
	model.email (String)
	model.messages_count (Number)
	pageHeader (String)
	collapseElements (Boolean)
	closeStatusId (Boolean)

----}}
