{{!
	© 2017 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
}}

<div class="menu-tree" data-type="menu-tree-root" data-view="MenuItems.Collection"></div>

<script>
	setTimeout(function(){
	jQuery('a[data-id="product"]').attr("href","https://tricerat.app.box.com/v/ProductDownloads");
	jQuery('a[data-id="feedback"]').attr("href","https://tricerat.co1.qualtrics.com/jfe/form/SV_agDiU3JLEpVsr7T");
	jQuery('a[data-id="ResellerRegistration"]').attr("href","http://www2.tricerat.com/l/17782/2017-10-03/cvfkwx");
	
	jQuery('a[data-id="product"]').attr("target","_blank");
	jQuery('a[data-id="feedback"]').attr("target","_blank");
	//jQuery('a[data-id="ActiveSupport"]').attr("target","_blank");
	jQuery('a[data-id="ResellerRegistration"]').attr("target","_blank");

	
	},1000)
</script>

{{!----
Use the following context variables when customizing this template: 
	
	menuItems (Array)

----}}
