/*
	© 2017 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
*/
//new update
// Case.js
// -----------------
// Defines the Case module. (Model, Views, Router)
// @module Case
define(
	'license'
,	[
		'SC.Configuration'
	,	'license.Router'

	,	'underscore'
	,	'Utils'
	]
,	function (
		Configuration
	,	Router

	,	_
	)
{
	'use strict';

	var case_menu_items = function (application)
	{
		if (!application.CaseModule.isEnabled())
		{
			return undefined;
		}

		return {
			id: 'licences'
		,	name: _('My Downloads').translate()
		,	url: 'showLicenseList'
		,	index: 7
		,	children:  [
				
			]
		,	permission: ''
		};
	};

	// Encapsulate all Case elements into a single module to be mounted to the application
	// Update: Keep the application reference within the function once its mounted into the application
	var licenceModule = function() 
	{
		// Is Case functionality available for this application?
		// var isCaseManagementEnabled = function ()
		// {
		// 	return SC && SC.ENVIRONMENT && SC.ENVIRONMENT.casesManagementEnabled;
		// };

		var mountToApp = function (application)
		{
			application.licenceModule = licenceModule;

			// always start our router.
			return new Router(application);
		};

		return {
			Router: Router
		,	isEnabled: true
		,	mountToApp: mountToApp
		,	MenuItems: case_menu_items
		};
	}();

	return licenceModule;
});