{{!
	© 2017 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
}}

{{#if showBackToAccount}}
	<a href="/" class="case-list-button-back">
		<i class="case-list-button-back-icon"></i>
		{{translate 'Back to Account'}}
	</a>
{{/if}}

<section class="case-list">
	<header class="case-list-header">
	
	</header>

	<div class="case-list-results-container">
		{{#if hasCases}}
	
			<table class="case-list-recordviews-table">
				<thead class="case-list-content-table">
					<tr class="case-list-content-table-header-row">
						<th class="case-list-content-table-header-row-title">
							<span>{{translate 'License Download.'}}</span>
						</th>
						<th class="case-list-content-table-header-row-subject">
							<span>{{translate 'Name'}}</span>
						</th>					
						
					</tr>
				</thead>
				<tbody data-view="Case.List.Items"></tbody>
				<script>
				setTimeout(function(){
					
						$('.recordviews-title-anchor').each(function(){
						var attr = $(this).attr("data-hashtag");
						$(this).attr("onclick","redirect('"+attr+"')")
						$(this).attr("data-hashtag","");
						})

						$('.recordviews-row').each(function(){
						var attr = $(this).attr("data-navigation-hashtag");
						$(this).attr("onclick","redirect('"+attr+"')")
						$(this).attr("data-navigation-hashtag","");
						})
						
				},1500);
				
				 function redirect(url)
				 {
					 
					 window.open(url,'_blank');
				 }
				</script>
			</table>
		{{else}}
			{{#if isLoading}}
				<p class="case-list-empty">{{translate 'Loading...'}}</p>
			{{else}}
				<p class="case-list-empty">{{translate 'No cases were found'}}</p>
			{{/if}}
		{{/if}}
	</div>

	{{#if showPagination}}
		<div class="case-list-paginator">
			<div data-view="GlobalViews.Pagination" class="case-list-global-views-pagination"></div>
			{{#if showCurrentPage}}
				<div data-view="GlobalViews.ShowCurrentPage" class="case-list-global-views-current-page"></div>
			{{/if}}
		</div>
	{{/if}}
</section>




{{!----
Use the following context variables when customizing this template: 
	
	pageHeader (String)
	hasCases (Number)
	isLoading (Boolean)
	showPagination (Boolean)
	showCurrentPage (Boolean)
	showBackToAccount (Boolean)

----}}
