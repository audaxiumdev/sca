{{!
	© 2017 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
}}

<section class="case-detail">
	<header class="case-detail-title">
		<a href="/cases" class="case-detail-back-btn">{{translate '&lt; Back to Cases'}}</a>
		<h2 class="case-detail-header-title">
			<span class="case-detail-field-number">{{pageHeader}}</span>
			<span class="case-detail-field-subject"> {{model.title}}</span>
		</h2>
	</header>

	<div data-confirm-message class="case-detail-confirm-message"></div>

	<div data-type="alert-placeholder"></div>
	{{debug model}}
	<div class="case-detail-header-information">
		<div class="case-detail-header-row">
			<div class="case-detail-header-col-left">

			</div>
	</div>

	<div class="">
		<ul class="list-group">
		  <li class="list-group-item"><b>End User</b>: {{model.endUser}}</li>
		  <li class="list-group-item"><b>Reseller</b>: {{model.licensereseller}}</li>
		  <li class="list-group-item"><b>Order Reference</b>: {{model.ordref}}</li>
		  <li class="list-group-item"><b>Serial Number</b>: {{model.serialNumber}}</li>
		  <li class="list-group-item"><b>Issue Date</b>: {{model.issueDate}}</li>
		  <li class="list-group-item"><b>License End Date</b>: {{model.lice_end_date}}</li>
		  <li class="list-group-item"><b>Entitlement Date</b>: {{model.version_entitle_date}}</li>
		  <li class="list-group-item"><b>Quantity</b>: {{model.lic_quantity}}</li>
		  <li class="list-group-item"><b>Product</b>: {{model.cc_product}}</li>
		  <li class="list-group-item"><b>Sales Order Contract</b>: {{model.cc_originalcontract}}</li>
		 
		</ul>
		<!-- <table class="table">
		  <thead>
		    <tr>
		      <th>End User</th>
		      <th>Reseller</th>
		      <th>Order Reference</th>
		      <th>Serial Number</th>
		      <th>Issue Date</th>
		    </tr>
		  </thead>
		  <tbody>
		    <tr>
		      <td>{{model.endUser}}</th>
		      <td>{{model.licensereseller}}</td>
		      <td>{{model.ordref}}</td>	
		      <td>{{model.serialNumber}}</td>  
		      <td>{{model.issueDate}}</td>      
		    </tr>
		  </tbody>
		</table> -->
</div>
<!-- 	<div class="">
		<table class="table">
		  <thead>
		    <tr>	  
		      <th>License End Date</th>
		      <th>Entitlement Date</th>		      
		      <th>Qunatity</th>
		      <th>Product</th>
		      <th>Sales Order Contract</th>
		    </tr>
		  </thead>
		  <tbody>
		    <tr>
		      <td>{{model.lice_end_date}}</td>
		      <td>{{model.version_entitle_date}}</td>
		      <td>{{model.lic_quantity}}</td>		      
		      <td>{{model.cc_product}}</td>
		      <td>{{model.cc_originalcontract}}</td>	      
		      
		    </tr>
		  </tbody>
		</table>
</div> -->


</section>



{{!----
Use the following context variables when customizing this template: 
	
	model (Object)
	model.internalid (String)
	model.caseNumber (String)
	model.title (String)
	model.grouped_messages (Array)
	model.grouped_messages.0 (Object)
	model.grouped_messages.0.date (String)
	model.grouped_messages.0.messages (Array)
	model.grouped_messages.0.messages.0 (Object)
	model.grouped_messages.0.messages.0.author (String)
	model.grouped_messages.0.messages.0.text (String)
	model.grouped_messages.0.messages.0.messageDate (String)
	model.grouped_messages.0.messages.0.internalid (String)
	model.grouped_messages.0.messages.0.initialMessage (Boolean)
	model.status (Object)
	model.status.id (String)
	model.status.name (String)
	model.origin (Object)
	model.origin.id (String)
	model.origin.name (String)
	model.category (Object)
	model.category.id (String)
	model.category.name (String)
	model.company (Object)
	model.company.id (String)
	model.company.name (String)
	model.priority (Object)
	model.priority.id (String)
	model.priority.name (String)
	model.createdDate (String)
	model.lastMessageDate (String)
	model.email (String)
	model.messages_count (Number)
	pageHeader (String)
	collapseElements (Boolean)
	closeStatusId (Boolean)

----}}
