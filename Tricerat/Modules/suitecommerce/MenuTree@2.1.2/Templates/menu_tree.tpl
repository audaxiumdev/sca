{{!
	© 2017 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
}}

<div class="menu-tree" data-type="menu-tree-root" data-view="MenuItems.Collection"></div>

<script>
	setTimeout(function(){
	jQuery('a[data-id="product"]').attr("href","https://tricerat.app.box.com/v/ProductDownloads");
	jQuery('a[data-id="feedback"]').attr("href","https://www.surveymonkey.com/r/T55VJKX");
	jQuery('a[data-id="ResellerRegistration"]').attr("href","https://go.tricerat.com/reseller-application");
	jQuery('a[data-id="RegisteraDeal"]').attr("href","https://go.tricerat.com/reseller-lead-registration");

	jQuery('a[data-id="product"]').attr("target","_blank");
	jQuery('a[data-id="feedback"]').attr("target","_blank");
	//jQuery('a[data-id="ActiveSupport"]').attr("target","_blank");
	jQuery('a[data-id="ResellerRegistration"]').attr("target","_blank");
	jQuery('a[data-id="RegisteraDeal"]').attr("target","_blank");


	},1000)
</script>

{{!----
Use the following context variables when customizing this template:

	menuItems (Array)

----}}
